# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: lperron <marvin@le-101.fr>                 +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/08/13 13:17:34 by lperron      #+#   ##    ##    #+#        #
#    Updated: 2018/12/12 11:45:30 by lperron     ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #



SRCDIRPR= ./srcs/
FILESPR= float_handler.c ft_printf.c percent_handler.c unsigned_handler.c ft_format_numb.c ft_printf_add.c string_handler.c utils.c ft_parser.c int_handler.c unico_handler.c widthprec.c bin_handler.c functions.c int_handler2.c
FILESINC= ft_printf.h
FILESINCLIB= libft.h
INCDIR= ./includes/
INCDIRLIB= ./libft/includes/
SRC= $(addprefix $(SRCDIRPR),$(FILESPR))
INC= $(addprefix $(INCDIR),$(FILESINC))
OBJ= $(SRC:.c=.o)
LIB= ./libft/libft.a
NAME= libftprintf.a
CFLAGS= -Wall -Werror -Wextra
CC= gcc

all: $(NAME)

%.o: %.c $(INC)
	$(CC) -c -o $@ $< -I $(INCDIR) -I $(INCDIRLIB)  $(CFLAGS)

$(LIB):
	make -C ./libft/

$(NAME): $(LIB) $(OBJ) $(INC)
	cp ./libft/libft.a $(NAME)
	ar -rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -f $(OBJ)
	make clean -C ./libft/

fclean: clean
	rm -f $(NAME) $(LIB)

re: fclean $(LIB) $(NAME)
